function load() {
	carte = Descartes.applyDescartesContext(context);
}

function addFeature1() {
    document.getElementById("addFeature1").disabled=true;
    
    var format = new ol.format.GeoJSON();
    var geojson = '{"crs":{"properties":{"name":"EPSG:4326"},"type":"name"},"features":[{"id":"dObjectId_1","properties":{"Nom":"NomA","Type": "Type A"},"type":"Feature","geometry":{"type":"Point","coordinates":[1.317688888888891,45.55597046260131]}}],"type":"FeatureCollection"}';

    var maCouche = carte.mapContent.getLayerById("maCoucheVectorA");
    maCouche.addFeatures(format.readFeatures(geojson));
    

}

function addFeature2() {
	document.getElementById("addFeature2").disabled=true;
	
    var format = new ol.format.GeoJSON();
    var geojson = '{"crs":{"properties":{"name":"EPSG:4326"},"type":"name"},"features":[{"id":"dObjectId_2","properties":{"Nom":"NomB","Type": "Type B"},"type":"Feature","geometry":{"type":"Point","coordinates":[0.3737481481481506,46.56841490704575]}}],"type":"FeatureCollection"}';

    var maCouche = carte.mapContent.getLayerById("maCoucheVectorB");
    maCouche.addFeatures(format.readFeatures(geojson));

}

function exportPNG() {
	carte.OL_map.once('postcompose', function(event) {
      var canvas = event.context.canvas;
      if (navigator.msSaveBlob) {
        navigator.msSaveBlob(canvas.msToBlob(), 'maCarteDescartes.png');
      } else {
        canvas.toBlob(function(blob) {
        	saveAs(blob, 'maCarteDescartes.png');
        });
      }
    });
	carte.OL_map.renderSync();
}

function exportPDF() {
	carte.OL_map.once('postcompose', function(event) {
  		var format="a4";
  		var resolution=72;
  		var dims = {
  			  a0: [1189, 841],
  			  a1: [841, 594],
  			  a2: [594, 420],
  			  a3: [420, 297],
  			  a4: [297, 210],
  			  a5: [210, 148],
  			};	
  		var dim = dims[format];
  	    var width = Math.round((dim[0] * resolution) / 25.4);
  	    var height = Math.round((dim[1] * resolution) / 25.4);
  	    var size = carte.OL_map.getSize();
  	    var viewResolution = carte.OL_map.getView().getResolution();

    	var canvas = event.context.canvas;
	    var pdf = new jspdf.jsPDF("landscape", undefined, format);
	    pdf.addImage(
	    	canvas.toDataURL("image/jpeg"),
	        "JPEG",
	        0,
	        0,
	        dim[0],
	        dim[1]
	    );
	    pdf.save("maCarteDescartes.pdf");
    });
	carte.OL_map.renderSync();
}