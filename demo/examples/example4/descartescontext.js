var context = {
	"map" : {
		"type": "Continuous",
		"div": "map",
		"mapParams": {
			"projection": "EPSG:4326",
			"displayExtendedOLExtent": true,
			"initExtent": [-0.615, 41.657, 5.721, 51.993],
			"maxExtent": [-0.615, 41.657, 5.721, 51.993],
			"minScale": null,
			"maxScale": 100,
			"autoSize": true
		}
	},
	"mapContent": {
		"items" : [
		    {
				"itemType" : "Layer",
				"title" : "Ma Couche Vector A",
				"type" : Descartes.Layer.TYPE_GenericVector,
				"options" : {
					"id" : "maCoucheVectorA",
					"queryable" : false,
			        "symbolizersFunction": function(feature) {
	    	        	var style = new ol.style.Style({
	    	                text: new ol.style.Text({
	    	                	text: feature.get("Nom"),
	    	                	font: 'normal 18px',
	  		                    fill: new ol.style.Fill({color: 'black'}),
	  		                    offsetX: 0,
	  		                    offsetY: 15
	  		                }),
	    	                image: new ol.style.Icon({
	    	                    src: 'marker2.png',
	    	                    anchorOrigin: "bottom-right"
	    	                })
	    	                /*image: new ol.style.Circle({
		  	                  radius: 5,
		  	                  fill: new ol.style.Fill({
		  	                    color: 'red'
		  	                  })
		  	                }),*/
	    	              });
	    	              return style;
				    }
		          
				}
			},
		    {
				"itemType" : "Layer",
				"title" : "Ma Couche Vector B",
				"type" : Descartes.Layer.TYPE_GenericVector,
				"options" : {
					"id" : "maCoucheVectorB",
					"queryable" : false,
			        "symbolizersFunction": function(feature) {
	    	        	var style = new ol.style.Style({
	    	                text: new ol.style.Text({
	    	                	text: feature.get("Nom"),
	    	                	font: 'normal 18px',
	  		                    fill: new ol.style.Fill({color: 'black'}),
	  		                    offsetX: 0,
	  		                    offsetY: 15
	  		                }),
	    	                image: new ol.style.Icon({
	    	                    src: 'marker3.png',
	    	                    anchorOrigin: "bottom-right"
	    	                })
	    	                /*image: new ol.style.Circle({
		  	                  radius: 5,
		  	                  fill: new ol.style.Fill({
		  	                    color: 'blue'
		  	                  })
		  	                }),*/
	    	              });
	    	              return style;
				    }
		          
				}
			},
			{
				"itemType" : "Layer",
				"title" : "OSM",
				"type" : Descartes.Layer.TYPE_OSM
			}
		]
	},
	"features": {
	    "selectToolTip":{
	    	"selectToolTipLayers": [
	    	    {
	    	        "layerId": "maCoucheVectorA", 
	    	        "fields": ['Nom','Type']
	    	    },
	    	    {
	    	    	"layerId": "maCoucheVectorB", 
	    	    	"fields": ['Nom']
	    	    }
	    	],
	    	"options": {}
	    },
		"toolBars": [
 		    {
 				"div": "toolBar",
 				"tools" : [
 				            {"type" : Descartes.Map.DRAG_PAN},
 							{"type" : Descartes.Map.ZOOM_IN},
 							{"type" : Descartes.Map.ZOOM_OUT},
 							{"type" : Descartes.Map.INITIAL_EXTENT, 
 							 args : [799205.2, 6215857.5, 1078390.1, 6452614.0]
 							},
 							{"type" : Descartes.Map.MAXIMAL_EXTENT},
 							{"type" : Descartes.Map.CENTER_MAP},
 							{"type" : Descartes.Map.COORDS_CENTER},
 							{"type" : Descartes.Map.NAV_HISTORY},
 							{"type" : Descartes.Map.DISTANCE_MEASURE},
 							{"type" : Descartes.Map.AREA_MEASURE},
 							{"type" : Descartes.Button.InkMapExportPNG}
 				],
 				"options": {}
 		    }
 		],
		"actions": [
            {
	            "type": Descartes.Action.InkMapParamsManager,
	            "div": 'Pdf',
	            "options": {
	                label: true,
	                optionsPanel: {
	                    collapsible: true,
	                    collapsed: true
	                },
	                params: {
	                    displayProjections: ["EPSG:2154","EPSG:4326"],
	                    selectedDisplayProjectionIndex:1
	                }
	            }
	        }
	   ],
	    "openlayersFeatures":{
	         "interactions":[ 
	               {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},
	               {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
	     	 ]
		}
	}
};
