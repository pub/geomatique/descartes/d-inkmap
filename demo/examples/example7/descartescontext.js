var context = {
	"map" : {
		"type": "Continuous",
		"div": "map",
		"mapParams": {
			"projection": "EPSG:3857",
			"displayExtendedOLExtent": true,
			"initExtent": [-513656.830076, 5963311.198696, -190786.822600, 6269059.311837],
			"maxExtent": [-513656.830076, 5963311.198696, -190786.822600, 6269059.311837],
			"minScale": null,
			"maxScale": 100,
			"autoSize": true
		}
  },
	"mapContent": {
    "items": [
      {
        "itemType": "Layer",
        "title": "Transport Bretagne",
        "type": 0,
        "options": {
          "format": "image/png",
          "legend": null,
          "metadataURL": null,
          "attribution": null,
          "visible": true,
          "alwaysVisible": false,
          "queryable": true,
          "activeToQuery": true,
          "sheetable": false,
          "opacity": 100,
          "opacityMax": 100,
          "displayOrder": 1,
          "addedByUser": false,
          "id": "DescartesLayer_m1thva",
          "hide": false,
          "maxScale": null,
          "minScale": null,
          "ratio": 1
        },
        "definition": [
          {
            "serverUrl": "https://ows.region-bretagne.fr/geoserver/rb/wms",
            "serverVersion": null,
            "layerName": "troncon_ferroviaire",
            "layerStyles": null,
            "imageServerUrl": null,
            "imageServerVersion": null,
            "imageLayerName": null,
            "imageLayerStyles": null,
            "featureServerUrl": null,
            "featureServerVersion": null,
            "featureName": null,
            "featurePrefix": null,
            "featureNameSpace": null,
            "featureGeometryName": "msGeometry",
            "featureLoaderMode": null,
            "featureReverseAxisOrientation": false,
            "featureInternalProjection": null,
            "crossOrigin": null,
            "internalProjection": null,
            "displayProjection": null,
            "useBboxSrsProjection": false,
            "displayMaxFeatures": null,
            "displayFeatures": null
          },
          {
            "serverUrl": "https://ows.region-bretagne.fr/geoserver/rb/wms",
            "serverVersion": null,
            "layerName": "arret_ferroviaire",
            "layerStyles": null,
            "imageServerUrl": null,
            "imageServerVersion": null,
            "imageLayerName": null,
            "imageLayerStyles": null,
            "featureServerUrl": null,
            "featureServerVersion": null,
            "featureName": null,
            "featurePrefix": null,
            "featureNameSpace": null,
            "featureGeometryName": "msGeometry",
            "featureLoaderMode": null,
            "featureReverseAxisOrientation": false,
            "featureInternalProjection": null,
            "crossOrigin": null,
            "internalProjection": null,
            "displayProjection": null,
            "useBboxSrsProjection": false,
            "displayMaxFeatures": null,
            "displayFeatures": null
          }
        ]
      },
      {
        "itemType": "Layer",
        "title": "Population Bretagne",
        "type": 0,
        "options": {
          "format": "image/png",
          "legend": null,
          "metadataURL": null,
          "attribution": null,
          "visible": true,
          "alwaysVisible": false,
          "queryable": true,
          "activeToQuery": true,
          "sheetable": false,
          "opacity": 50,
          "opacityMax": 100,
          "displayOrder": 2,
          "addedByUser": false,
          "id": "DescartesLayer_nq3n5n",
          "hide": false,
          "maxScale": null,
          "minScale": null,
          "ratio": 1
        },
        "definition": [
          {
            "serverUrl": "https://ows.region-bretagne.fr/geoserver/rb/wms",
            "serverVersion": null,
            "layerName": "rp_struct_pop_geom",
            "layerStyles": null,
            "imageServerUrl": null,
            "imageServerVersion": null,
            "imageLayerName": null,
            "imageLayerStyles": null,
            "featureServerUrl": null,
            "featureServerVersion": null,
            "featureName": null,
            "featurePrefix": null,
            "featureNameSpace": null,
            "featureGeometryName": "msGeometry",
            "featureLoaderMode": null,
            "featureReverseAxisOrientation": false,
            "featureInternalProjection": null,
            "crossOrigin": null,
            "internalProjection": null,
            "displayProjection": null,
            "useBboxSrsProjection": false,
            "displayMaxFeatures": null,
            "displayFeatures": null
          }
        ]
      },
      {
        "itemType": "Layer",
        "title": "Lycee",
        "type": 4,
        "options": {
          "format": "image/png",
          "legend": null,
          "metadataURL": null,
          "attribution": null,
          "visible": true,
          "alwaysVisible": false,
          "queryable": true,
          "activeToQuery": true,
          "sheetable": false,
          "opacity": 100,
          "opacityMax": 100,
          "displayOrder": 3,
          "addedByUser": false,
          "id": "DescartesLayer_oa4l7",
          "hide": false,
          "maxScale": null,
          "minScale": null,
          "symbolizers": {
            "Point": {
              "pointRadius": 4,
              "graphicName": "circle",
              "fillColor": "#666666",
              "fillOpacity": 1,
              "strokeWidth": 1,
              "strokeOpacity": 1,
              "strokeColor": "#666666",
              "pointerEvents": "visiblePainted",
              "cursor": "pointer"
            },
            "Line": {
              "fillColor": "#666666",
              "fillOpacity": 1,
              "strokeWidth": 3,
              "strokeOpacity": 1,
              "strokeColor": "#666666",
              "strokeDashstyle": "solide",
              "pointerEvents": "visiblePainted",
              "cursor": "pointer"
            },
            "Polygon": {
              "strokeWidth": 2,
              "strokeOpacity": 1,
              "strokeColor": "#666666",
              "fillColor": "#666666",
              "fillOpacity": 0.3,
              "pointerEvents": "visiblePainted",
              "cursor": "pointer"
            },
            "MultiPoint": {
              "pointRadius": 4,
              "graphicName": "circle",
              "fillColor": "#666666",
              "fillOpacity": 1,
              "strokeWidth": 1,
              "strokeOpacity": 1,
              "strokeColor": "#666666",
              "pointerEvents": "visiblePainted",
              "cursor": "pointer"
            },
            "MultiLine": {
              "fillColor": "#666666",
              "fillOpacity": 1,
              "strokeWidth": 3,
              "strokeOpacity": 1,
              "strokeColor": "#666666",
              "strokeDashstyle": "solide",
              "pointerEvents": "visiblePainted",
              "cursor": "pointer"
            },
            "MultiPolygon": {
              "strokeWidth": 2,
              "strokeOpacity": 1,
              "strokeColor": "#666666",
              "fillColor": "#666666",
              "fillOpacity": 0.3,
              "pointerEvents": "visiblePainted",
              "cursor": "pointer"
            }
          },
          "symbolizersFunction": null
        },
        "definition": [
          {
            "serverUrl": "https://ows.region-bretagne.fr/geoserver/rb/wfs",
            "serverVersion": "1.0.0",
            "layerName": "lycee",
            "layerStyles": null,
            "imageServerUrl": null,
            "imageServerVersion": null,
            "imageLayerName": null,
            "imageLayerStyles": null,
            "featureServerUrl": null,
            "featureServerVersion": null,
            "featureName": "lycee",
            "featurePrefix": "rb",
            "featureNameSpace": null,
            "featureGeometryName": "geom",
            "featureLoaderMode": null,
            "featureReverseAxisOrientation": false,
            "featureInternalProjection": null,
            "crossOrigin": null,
            "internalProjection": null,
            "displayProjection": null,
            "useBboxSrsProjection": false,
            "displayMaxFeatures": null,
            "displayFeatures": null
          }
        ]
      }
    ],
    "bbox": {
      "xMin": -539645.4196934332,
      "yMin": 5963311.198696,
      "xMax": -164798.23298256684,
      "yMax": 6269059.311837
    },
    "size": {
      "w": 613,
      "h": 500
    }
  },
  "features": {
    "toolBars": [
      {
      "div": "toolBar",
      "tools" : [
            {"type" : Descartes.Map.DRAG_PAN},
            {"type" : Descartes.Map.ZOOM_IN},
            {"type" : Descartes.Map.ZOOM_OUT},
            {"type" : Descartes.Map.INITIAL_EXTENT, 
             args : [799205.2, 6215857.5, 1078390.1, 6452614.0]
            },
            {"type" : Descartes.Map.MAXIMAL_EXTENT},
            {"type" : Descartes.Map.CENTER_MAP},
            {"type" : Descartes.Map.COORDS_CENTER},
            {"type" : Descartes.Map.NAV_HISTORY},
            {"type" : Descartes.Map.DISTANCE_MEASURE},
            {"type" : Descartes.Map.AREA_MEASURE},
            {"type" : Descartes.Button.InkMapExportPNG}
      ],
      "options": {}
      }
    ],
    "actions": [
    {
      "type": Descartes.Map.SCALE_SELECTOR_ACTION,
            "div": 'ScaleSelector',
            "options": {
                label: true,
                optionsPanel: {
                    collapsible: true,
                    collapsed: true
                }
            }
        }, {
            "type": Descartes.Action.InkMapParamsManager,
            "div": 'Pdf',
            "options": {
                label: true,
                optionsPanel: {
                    collapsible: true,
                    collapsed: true
                },
                params: {
                    displayProjections: ["EPSG:3857","EPSG:2154","EPSG:4326"]
                }
            }
        }
    ],
    "openlayersFeatures":{
        "interactions":[ 
              {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},
              {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}
    	 ]
	}
  }
  
}
