proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('http://www.opengis.net/gml/srs/epsg.xml#2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:ogc:def:crs:EPSG::2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");


//overwrite symbolizers for testing
Descartes.Symbolizers.Descartes_Symbolizers_WFS = {
    'Point': {
        pointRadius: 5,
        graphicName: 'circle',
        fillColor: '#e32d19',
        fillOpacity: 0.5,
        strokeWidth: 2,
        strokeOpacity: 1,
        strokeColor: '#666666',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#e32d19',
        fillOpacity: 1,
        strokeWidth: 5,
        strokeOpacity: 0.5,
        strokeColor: '#156fe6',
        strokeLinecap: 'round',
        strokeDashstyle: [10, 10],
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2,
        strokeOpacity: 0.5,
        strokeColor: '#1da15d',
        fillColor: '#1da15d',
        fillOpacity: 0.3,
        strokeDashstyle: [10, 10],
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

Descartes.Symbolizers.Descartes_Symbolizers_WFS['MultiPoint'] = _.extend(Descartes.Symbolizers.Descartes_Symbolizers_WFS['Point'], {});
Descartes.Symbolizers.Descartes_Symbolizers_WFS['MultiLine'] = _.extend(Descartes.Symbolizers.Descartes_Symbolizers_WFS['Line'], {});
Descartes.Symbolizers.Descartes_Symbolizers_WFS['MultiPolygon'] = _.extend(Descartes.Symbolizers.Descartes_Symbolizers_WFS['Polygon'], {});

var load = function () {
	
    Descartes.Log.setLevel('debug');
    
	var bounds = [-101991.9, 6023917.0, 1528303.1, 7110780.4];
    var projection = "EPSG:2154";
    
    /*var couchePoints = new Descartes.Layer.WFS('Points', [{
        serverUrl: 'http://mapserveur.application.developpement-durable.gouv.fr/map/mapserv?map=/opt/data/carto/cartelie/prod/PNE_IG/OSM_2.www.map',
        layerName: 'c_stations',
        featureName: 'c_stations',
        serverVersion: '1.1.0',
    }]);*/

    var coucheLines = new Descartes.Layer.WFS('Lines', [{
    	serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560/ows?",
		layerName: 'L_PISTE_SKI_L_074',
        serverVersion: '1.1.0',
        featureNameSpace:"org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
    	featureGeometryName: "the_geom"
    }]);

    var couchePolygons = new Descartes.Layer.WFS('Polygons', [{
		serverUrl: "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560/ows?",
		layerName: "L_ENV_DOMAINE_SKIABLE_S_074",
		serverVersion: "1.1.0",
		featureNameSpace:"org_100000002_f0443c65-01aa-417b-bde2-f0bdc9393560",
		featureGeometryName: "the_geom",
    }]);

    var contenuCarte = new Descartes.MapContent({
        editable: true,
        editInitialItems: false,
        fixedDisplayOrders: false,
        fctQueryable: true,
        fctVisibility: true,
        fctOpacity: true,
        displayIconLoading: true,
        fctContextMenu: false,
        fctDisplayLegend: true,
        displayMoreInfos: true
    });

    var mapOptions = {
        units: 'm',
        projection: projection,
        initExtent: bounds,
        maxExtent: bounds,
        autoSize: true
    };

    carte = new Descartes.Map.ContinuousScalesMap('map', contenuCarte, mapOptions);

    manager = carte.addContentManager("layersTree");

    //contenuCarte.addItem(couchePoints);
    contenuCarte.addItem(coucheLines);
    contenuCarte.addItem(couchePolygons);

    carte.show();

    var label = true;
    var collapsible = true;
    var collapsed = false;

    var actions = [{
            type: "ScaleSelector",
            div: 'ScaleSelector',
            options: {
                label: label,
                optionsPanel: {
                    collapsible: collapsible,
                    collapsed: collapsed
                }
            }
        }, {
            type: Descartes.Action.InkMapParamsManager,
            div: 'Pdf',
            options: {
                label: label,
                optionsPanel: {
                    collapsible: collapsible,
                    collapsed: collapsed
                },
                params: {
                    displayProjections: ["EPSG:2154","EPSG:4326"]
                }
            }
        }
    ];

    carte.addActions(actions);

    //TOOLBAR
    var principalesTools = [
        {type: Descartes.Map.DRAG_PAN},
        {type: Descartes.Button.InkMapExportPDF},
        {type: Descartes.Button.InkMapExportPNG},
    ];
    var toolsBar = carte.addNamedToolBar("toolbar", principalesTools);

    //DIRECTIONPANPANEL
    panPanel = carte.addDirectionalPanPanel({duration: 500});

    carte.addMiniMap("http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geocat_metr.map&LAYERS=raster_geosignal", {open: false, version: '1.1.1'});

    carte.addOpenLayersInteractions([
        {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},
        {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}]);

};
