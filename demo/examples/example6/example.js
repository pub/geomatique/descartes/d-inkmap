var load = function () {
	
    Descartes.Log.setLevel('debug');

    var projection = new Descartes.Projection('EPSG:3857');

    //add WMTS IGN
    var coucheWMTSSpec= {
	    "title": "Photographies aériennes IGN",
	    "options": {
	        "visible": true,
	        "opacity": 50,
	        "minScale": null,
	        "maxScale": null,
	        "matrixSet": "PM",
	        "projection": "EPSG:3857",
	        "matrixIds": ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21"],
	        "origins": [[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],],
	        "resolutions": [156543.033928041,78271.51696402048,39135.758482010235,19567.87924100512,9783.93962050256,4891.96981025128,2445.98490512564,1222.99245256282,611.49622628141,305.7481131407048,152.8740565703525,76.43702828517624,38.21851414258813,19.10925707129406,9.554628535647032,4.777314267823516,2.388657133911758,1.194328566955879,0.5971642834779395,0.2985821417389697,0.1492910708694849,0.0746455354347424],
	        "tileSizes": [256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256],
	        "format": "image/jpeg",
			"attribution": "©IGN",
	        "queryable": false
	    },
	    "definition": [
	        {
	            "serverUrl": "https://wxs.ign.fr/pratique/geoportail/wmts?",
	            "serverVersion": "1.0.0",
	            "layerName": "ORTHOIMAGERY.ORTHOPHOTOS",
	            "extent": [-19981848.597392607,-12932243.11199203,19981848.597392607,12932243.11199202],
	            "layerStyles": "normal"
	        }
	    ]
	};
	
	var coucheWMTS = new Descartes.Layer.WMTS(coucheWMTSSpec.title, coucheWMTSSpec.definition, coucheWMTSSpec.options);

    //add WMTS
//    var projectionExtent = projection.getExtent();
//    var tileSize = 256;
//    var size = ol.extent.getWidth(projectionExtent) / tileSize;
//    var origin = ol.extent.getTopLeft(projectionExtent);
//    var resolutions = new Array(14);
//    var matrixIds = new Array(14);
//    for (var z = 0; z < 14; ++z) {
//        // generate resolutions and matrixIds arrays for this WMTS
//        resolutions[z] = size / Math.pow(2, z);
//        matrixIds[z] = z;
//    }
//    var coucheWMTS = new Descartes.Layer.WMTS('Population USA', [{
//        serverUrl: 'https://services.arcgisonline.com/arcgis/rest/services/Demographics/USA_Population_Density/MapServer/WMTS/',
//        layerName: '0',
//    }],{
//        extent: [-13394213.340468, 3771708.723704, -6834081.824921, 6036690.745850],
//        matrixSet: "EPSG:3857",
//        projection: projection,
//        matrixIds: matrixIds,
//        origin: origin,
//        resolutions: resolutions,
//        tileSize: [256, 256],
//        format: 'image/png',
//        visible: true,
//        opacity: 50
//    });

    //WMTS works in browser but returns 403 Forbidden in client
    // var coucheWMTS = new Descartes.Layer.WMTS('Orthophotos', [{
    //     serverUrl: 'https://geobretagne.fr/geoportail/wmts',
    //     layerName: 'ORTHOIMAGERY.ORTHOPHOTOS',
    //     layerStyles: "normal"
    // }],{
    //     extent: [-513656.830076, 5963311.198696, -190786.822600, 6269059.311837],
    //     matrixSet: "PM",
    //     projection: projection,
    //     matrixIds: matrixIds,
    //     origin: origin,
    //     resolutions: resolutions,
    //     tileSize: [256, 256],
    //     format: 'image/jpeg',
    //     visible: true,
    //     opacity: 100
    // });

    //WMTS displays, but does not print
    // var resolutions = new Array(20);
    // var matrixIds = new Array(20);
    // for (var z = 0; z < 20; ++z) {
    //     // generate resolutions and matrixIds arrays for this WMTS
    //     resolutions[z] = size / Math.pow(2, z);
    //     matrixIds[z] = "EPSG:3857:" + z;
    // }
    // var coucheWMTS = new Descartes.Layer.WMTS('Carte IGN', [{
    //     serverUrl: 'http://tile.geobretagne.fr/gwc02/service/wmts',
    //     layerName: 'carte'
    // }],{
    //     extent: [-513656.830076, 5963311.198696, -190786.822600, 6269059.311837],
    //     matrixSet: "EPSG:3857",
    //     projection: projection,
    //     matrixIds: matrixIds,
    //     origin: origin,
    //     resolutions: resolutions,
    //     tileSize: [256, 256],
    //     format: 'image/jpeg',
    //     visible: true,
    //     opacity: 100
    // });

    var contenuCarte = new Descartes.MapContent({
        editable: true,
        editInitialItems: false,
        fixedDisplayOrders: false,
        fctQueryable: true,
        fctVisibility: true,
        fctOpacity: true,
        displayIconLoading: true,
        fctContextMenu: false,
        fctDisplayLegend: true,
        displayMoreInfos: true
    });

    //var extent3857US = [-13394213.340468, 3771708.723704, -6834081.824921, 6036690.745850];
    //var extent3857Bretagne = [-513656.830076, 5963311.198696, -190786.822600, 6269059.311837];
    var extent = [333837, 5315435, 1014141, 5592847];
            
    var mapOptions = {
        projection: projection,
        initExtent: extent,
        maxExtent: extent,
        autoSize: true
    };

    carte = new Descartes.Map.ContinuousScalesMap('map', contenuCarte, mapOptions);

    manager = carte.addContentManager("layersTree");

    
    contenuCarte.addItem(coucheWMTS);

    carte.show();

    var label = true;
    var collapsible = true;
    var collapsed = false;

    var actions = [{
            type: "ScaleSelector",
            div: 'ScaleSelector',
            options: {
                label: label,
                optionsPanel: {
                    collapsible: collapsible,
                    collapsed: collapsed
                }
            }
        }, {
            type: Descartes.Action.InkMapParamsManager,
            div: 'Pdf',
            options: {
                label: label,
                optionsPanel: {
                    collapsible: collapsible,
                    collapsed: collapsed
                },
                params: {
                    displayProjections: ["EPSG:3857","EPSG:2154","EPSG:4326"]
                }
            }
        }
    ];

    carte.addActions(actions);

    //TOOLBAR
    var principalesTools = [
        {type: Descartes.Map.DRAG_PAN},
        {type: Descartes.Button.InkMapExportPNG},
    ];
    carte.addNamedToolBar("toolbar", principalesTools);

    //DIRECTIONPANPANEL
    panPanel = carte.addDirectionalPanPanel({duration: 500});

    carte.addMiniMap("http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geocat_metr.map&LAYERS=raster_geosignal", {open: false, version: '1.1.1'});

    carte.addOpenLayersInteractions([
        {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},
        {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}]);

};
