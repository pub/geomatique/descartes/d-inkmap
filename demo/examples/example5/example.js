var load = function () {
	
    Descartes.Log.setLevel('debug');

    var projection = new Descartes.Projection('EPSG:2154');

    //add some WMS layers with 'Access-Control-Allow-Origin: *'
    var couchePopulation = new Descartes.Layer.WMS('Population Bretagne', [{
        serverUrl: 'https://ows.region-bretagne.fr/geoserver/rb/wms',
        layerName: 'rp_struct_pop_geom',
    }],{
        opacity: 50,
        legend:["https://ows.region-bretagne.fr/geoserver/rb/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rp_struct_pop_geom"]
    });
    var coucheTransport = new Descartes.Layer.WMS('Transport Bretagne', [{
        serverUrl: 'https://ows.region-bretagne.fr/geoserver/rb/wms',
        layerName: 'voie_navigable',
    },{
        serverUrl: 'https://ows.region-bretagne.fr/geoserver/rb/wms',
        layerName: 'arret_ferroviaire',
    }],
    {
    	legend:["https://ows.region-bretagne.fr/geoserver/rb/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=voie_navigable","https://ows.region-bretagne.fr/geoserver/rb/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=arret_ferroviaire"]
    });

    //add WFS (prints but does not display due to GML parsing bug in ol 4.6.5 ?)
    var coucheLycesWFS = new Descartes.Layer.WFS('Lycee', [{
        serverUrl: 'https://ows.region-bretagne.fr/geoserver/rb/wfs',
        layerName: 'lycee',
        featureName: 'lycee',
        featurePrefix: 'rb',
        featureGeometryName: 'geom',
        serverVersion: '1.0.0'
    }],{
        useProxy: false,
    });

    var contenuCarte = new Descartes.MapContent({
        editable: true,
        editInitialItems: false,
        fixedDisplayOrders: false,
        fctQueryable: true,
        fctVisibility: true,
        fctOpacity: true,
        displayIconLoading: true,
        fctContextMenu: false,
        fctDisplayLegend: true,
        displayMoreInfos: true
    });

    var extent2154 = [80343.84, 6727531.13, 404273.81, 6901594.77];
    // var extent3857 = [-513656.830076, 5963311.198696, -190786.822600, 6269059.311837];

    var mapOptions = {
        units: 'm',
        projection: projection,
        initExtent: extent2154,
        maxExtent: extent2154,
        autoSize: true
    };

    carte = new Descartes.Map.ContinuousScalesMap('map', contenuCarte, mapOptions);

    manager = carte.addContentManager("layersTree");

    contenuCarte.addItem(coucheTransport);
    contenuCarte.addItem(couchePopulation);
    contenuCarte.addItem(coucheLycesWFS);

    carte.show();

    var label = true;
    var collapsible = true;
    var collapsed = false;

    var actions = [{
            type: "ScaleSelector",
            div: 'ScaleSelector',
            options: {
                label: label,
                optionsPanel: {
                    collapsible: collapsible,
                    collapsed: collapsed
                }
            }
        }, {
            type: Descartes.Action.InkMapParamsManager,
            div: 'Pdf',
            options: {
                label: label,
                optionsPanel: {
                    collapsible: collapsible,
                    collapsed: collapsed
                },
                params: {
                    displayProjections: ["EPSG:2154","EPSG:4326"]
                }
            }
        }
    ];

    carte.addActions(actions);

    //TOOLBAR
    var principalesTools = [
        {type: Descartes.Map.DRAG_PAN},
        {type: Descartes.Button.InkMapExportPNG},
    ];
    carte.addNamedToolBar("toolbar", principalesTools);

    //DIRECTIONPANPANEL
    panPanel = carte.addDirectionalPanPanel({duration: 500});

    carte.addMiniMap("http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geocat_metr.map&LAYERS=raster_geosignal", {open: false, version: '1.1.1'});

    carte.addOpenLayersInteractions([
        {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},
        {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}]);

};
