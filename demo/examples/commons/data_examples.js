/* global Descartes, ol, proj4 */

var lamber93 = 'EPSG:2154';
var wgs84 = 'EPSG:4326';


proj4.defs('EPSG:2154', "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:3857", "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs");

var bounds_4326 = [4.2215633, 43.195563, 5.8258168, 43.8944053];
var bounds_2154 = [799356.1, 6233695.5, 927019.44, 6314662.5];


//var bounds = ol.extent.createOrUpdate(bounds_4326[0], bounds_4326[1], bounds_4326[2], bounds_4326[3]);
//console.log(bounds);

var groupeEspaces = {
    title: "Espaces protégés",
    options: {
        opened: true
    }
};

var groupeFonds = {
    title: "Fonds cartographiques",
    options: {
        opened: false
    }
};

var groupeRisques = {
    title: "Risques naturels",
    options: {
        opened: true
    }
};



var coucheCommunes = {
    title: "Limites des communes",
    definition: [
        {
            serverUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geolWMS.map&",
            layerName: "IGN_CARTO_COM",
            featureServerUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geolWMS.map&",
            featureName: "IGN_CARTO_COM"
        }
    ],
    options: {
        maxScale: 10000, // ex  50000
        minScale: 500000, // ex 500000
        alwaysVisible: false,
        visible: false,
        queryable: true,
        activeToQuery: false,
        sheetable: false,
        opacity: 100,
        opacityMax: 100,
        legend: ["http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geolWMS.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&layer=IGN_CARTO_COM"],
        metadataURL: null,
        format: "image/png",
        displayOrder: 6
    }
};

var coucheFailles = {
    title: "Failles",
    definition: [
        {
            serverUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/risquesIT.map&",
            layerName: "DAT_NEPAL_FAIL",
            serverVersion: "1.3.0",
            styles: "default",
            featureServerUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/risquesIT.map&",
            featureName: "DAT_NEPAL_FAIL"
        }
    ],
    options: {
        maxScale: 2000, // ex  50000
        minScale: 15000000, // ex 500000
        alwaysVisible: false,
        visible: true,
        queryable: true,
        activeToQuery: false,
        sheetable: true,
        opacity: 100,
        opacityMax: 100,
        legend: ["http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/risquesIT.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&layer=DAT_NEPAL_FAIL"],
        metadataURL: null,
        format: "image/png",
        displayOrder: 3
    }
};

var coucheLittoral = {
    title: "Conservatoire du littoral",
    definition: [
        {
            serverUrl: "http://ws.carmencarto.fr/WMS/119/fxx_inpn?",
            layerName: "Terrains_du_Conservatoire_du_Littoral",
            featureServerUrl: "http://ws.carmencarto.fr/WMS/119/fxx_inpn?",
            featureName: "Terrains_du_Conservatoire_du_Littoral"
        }
    ],
    options: {
        maxScale: 2000, // ex  50000
        minScale: 10000000, // ex 500000
        alwaysVisible: false,
        visible: true,
        queryable: true,
        activeToQuery: false,
        sheetable: false,
        opacity: 100,
        opacityMax: 100,
        legend: ["http://ws.carmencarto.fr/WMS/119/fxx_inpn?version=1.1.1&service=WMS&request=GetLegendGraphic&layer=Terrains_du_Conservatoire_du_Littoral&format=image/png"],
        metadataURL: null,
        format: "image/gif",
        displayOrder: 2
    }
};

var coucheGeosignal = {
    title: "Carte de situation",
    definition: [
        {
            serverUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geocat_metr.map&",
            layerName: "raster_geosignal"
        }
    ],
    options: {
        maxScale: 2000, // ex  50000
        minScale: null, // ex 500000
        alwaysVisible: false,
        visible: true,
        queryable: false,
        activeToQuery: false,
        sheetable: false,
        opacity: 100,
        opacityMax: 100,
        legend: null,
        metadataURL: null,
        format: "image/png",
        displayOrder: 7,
        attribution: "mon attribution"
    }
};

var coucheMines = {
    title: "Mines",
    definition: [
        {
            serverUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/sigmines_gites_mines.map&",
            layerName: "DAT_MINES,DAT_MINES_EMP",
            featureServerUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/sigmines_gites_mines.map&",
            featureName: "DAT_MINES,DAT_MINES_EMP"
        }
    ],
    options: {
        maxScale: 2000, // ex  50000
        minScale: null, // ex 500000
        alwaysVisible: false,
        visible: true,
        queryable: true,
        activeToQuery: false,
        sheetable: false,
        opacity: 100,
        opacityMax: 100,
        legend: ["http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/sigmines_gites_mines.map&request=GetLegendGraphic&SERVICE=WMS&VERSION=1.1.1&FORMAT=image/png&LAYER=DAT_MINESSSSS"],
        metadataURL: null,
        format: "image/png",
        displayOrder: 1
    }
};

var coucheParcs = {
    title: "Parc naturel régional",
    definition: [
        {
            serverUrl: "http://ws.carmencarto.fr/WMS/119/fxx_inpn?",
            layerName: "Parcs_naturels_regionaux",
            featureServerUrl: "http://ws.carmencarto.fr/WMS/119/fxx_inpn?",
            featureName: "Parcs_naturels_regionaux"
        }
    ],
    options: {
        maxScale: 2000, // ex  50000
        minScale: 10000000, // ex 500000
        alwaysVisible: false,
        visible: true,
        queryable: true,
        activeToQuery: true,
        sheetable: true,
        opacity: 50,
        opacityMax: 100,
        legend: ["http://ws.carmencarto.fr/WMS/119/fxx_inpn?version=1.1.1&service=WMS&request=GetLegendGraphic&layer=Parcs_naturels_regionaux&format=image/png"],
        metadataURL: "http://localhost:8080/cartelie/voir.do?carte=types_vecteur_noir_et_blanc&amp;service=Recette_technique",
        format: "image/gif",
        displayOrder: 5,
        attribution: "mon attribution 2"
    }
};

var coucheTerrain = {
    title: "Mouvements de terrain",
    definition: [
        {
            serverUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/risquesIT.map&",
            layerName: "DAT_MVT_FS_2",
            featureServerUrl: "http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/risquesIT.map&",
            featureName: "DAT_MVT_FS_2"
        }
    ],
    options: {
        maxScale: 2000, // ex  50000
        minScale: null, // ex 500000
        alwaysVisible: false,
        visible: true,
        queryable: true,
        activeToQuery: true,
        sheetable: false,
        opacity: 100,
        opacityMax: 100,
        legend: ["http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/risquesIT.map&request=GetLegendGraphic&SERVICE=WMS&VERSION=1.1.1&FORMAT=image/png&LAYER=DAT_MVT_FS_2"],
        metadataURL: null,
        format: "image/png",
        displayOrder: 4,
        attribution: "Attribution des mouvements de terrain"
    }
};


	var serveur = "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?";

	coucheNature = new Descartes.Layer.WMS(
		"Espaces naturels", 
		[
			{
				serverUrl: serveur,
				layerName: "c_natural_Valeurs_type"			}
			],
		{
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_natural_Valeurs_type"],
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);

	coucheBati = new Descartes.Layer.WMS(
		"Constructions", 
		[
			{
				serverUrl: serveur,
				layerName: "c_buildings",
				featureServerUrl: serveur,
				featureName: "c_buildings2",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		{
			maxScale: null,
			minScale: 39000,
			alwaysVisible: false,
			visible: true,
			queryable:false,
			activeToQuery:false,
			sheetable:false,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_buildings"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);


	coucheStations = new Descartes.Layer.WMS(
		"Stations essence", 
		[
			{
				serverUrl: serveur,
				layerName: "c_stations",
				featureServerUrl: serveur,
				featureName: "c_stations",
				featureGeometryName: "the_geom",
				internalProjection: "EPSG:3857",
				//useBboxSrsProjection: true,
				featureNameSpace: "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d",
				featureServerVersion: "1.1.0",
				serverVersion: "1.3.0"
			}
			],
		{
			maxScale: null,
			minScale: 390000,
			alwaysVisible: false,
			visible: true,
			queryable:true,
			activeToQuery:true,
			sheetable:true,
			opacity: 100,
			opacityMax: 100,
			legend: [serveur + "SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/png&LAYER=c_stations"],
			metadataURL: null,
			format: "image/png",
			attribution: "&#169;mon copyright"
		}
	);



