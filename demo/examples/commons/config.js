//Descartes.setWebServiceInstance('preprod'); //preprod ou localhost (default: prod)
//Descartes.setGeoRefWebServiceInstance('preprod'); //integration ou preprod (default: prod)
//var descartesUrlRoot = "https://preprod.descartes.din.developpement-durable.gouv.fr";
var descartesUrlRoot = "https://descartes.din.developpement-durable.gouv.fr";

var projection = 'EPSG:2154';
var initBounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];
var maxBounds = [799205.2, 6215857.5, 1078390.1, 6452614.0];

var urlParams = new URLSearchParams(window.location.search);
var moteurCarto = urlParams.get('moteurCarto');
var mapContentType = urlParams.get('mapContentType');

if (moteurCarto === null && mapContentType === null) {
	moteurCarto = "d-mapserver";
	mapContentType = "WMS";
}

var serveur;
var featureGeometryName;
var featureNameSpace;

switch (moteurCarto) {
  case 'carto2':
    serveur = "https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?";
	featureGeometryName = "the_geom";
	featureNameSpace = "org_4952483_22c7124f-8453-4535-b735-555f03d2c88d";
    break;
  case 'd-mapserver':
    serveur = descartesUrlRoot + "/mapserver?";
    featureGeometryName = "ms:geometry";
    featureNameSpace = "";
    break;
  case 'd-geoserver':
  	serveur = descartesUrlRoot + "/geoserver/ows?";
	featureGeometryName = "the_geom";
	featureNameSpace = "descartes";
    break;
  case 'd-qgisserver':
    serveur = descartesUrlRoot + "/qgisserver?";
	featureGeometryName = "geometry";
	featureNameSpace = "";  
    break;
  case 'd-mapserver-local':
	serveur = "http://localhost:8082/mapserver?";
	featureGeometryName = "ms:geometry";
	featureNameSpace = "";
    break;
  case 'd-geoserver-local':
	serveur = "http://localhost:8081/geoserver/ows?";
	featureGeometryName = "the_geom";
	featureNameSpace = "descartes";
    break;
  case 'd-qgisserver-local':
	serveur = "http://localhost:8083/qgisserver?";
	featureGeometryName = "geometry";
	featureNameSpace = "";   
    break;
  default:
	serveur = descartesUrlRoot + "/mapserver?";
    featureGeometryName = "ms:geometry";
    featureNameSpace = "";
}

var context = {};
function loadContext() {
	
	// constitution de l'objet JSON de contexte
	var contextFile = (new Descartes.Url(this.location.href)).getParamValue("context");
	
	if (contextFile !== "") {
		// contexte sauvegardé
		var xhr = new XMLHttpRequest();
        xhr.open('GET', Descartes.CONTEXT_MANAGER_SERVER+"?" + contextFile);
        xhr.onload = function () {
            if (xhr.status === 200) {
            	getContext(xhr);
            } else {
            	stopMap();
            }
        };
        xhr.send();
	
	} else {
		context.bbox = {xMin:initBounds[0], yMin:initBounds[1], xMax:initBounds[2], yMax:initBounds[3]};
		//context.size = {w:600 , h:400};
		context.items = [];
		
		if (mapContentType === "WMS") {
			context.items[0] = Descartes.Utils.extend(coucheToponymesSpecWms, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(groupeInfrasSpec, {itemType:"Group", items:[]});
			context.items[1].items[0] = Descartes.Utils.extend(coucheParkingsSpecWms, {itemType:"Layer"});
			context.items[1].items[1] = Descartes.Utils.extend(coucheStationsSpecWms, {itemType:"Layer"});
			context.items[1].items[2] = Descartes.Utils.extend(coucheRoutesSpecWms, {itemType:"Layer"});
			context.items[1].items[3] = Descartes.Utils.extend(coucheFerSpecWms, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(coucheEauSpecWms, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(coucheBatiSpecWms, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(coucheNatureSpecWms, {itemType:"Layer"});
			context.items[5] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[5].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[5].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[5].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[5].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[5].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});	
		} else if (mapContentType === "WFS") {
			context.items[0] = Descartes.Utils.extend(coucheParkingsSpecWfs, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(coucheStationsSpecWfs, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(coucheEauSpecWfs, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(coucheNatureSpecWfs, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(coucheBatiSpecWfs, {itemType:"Layer"});
			context.items[5] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[5].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[5].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[5].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[5].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[5].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});	
		} else if (mapContentType === "KML") {
			context.items[0] = Descartes.Utils.extend(coucheParkingsSpecKml, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(coucheStationsSpecKml, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[3].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[3].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[3].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[3].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[3].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});	
		} else if (mapContentType === "GEOJSON") {
			context.items[0] = Descartes.Utils.extend(coucheParkingsSpecGeoJson, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(coucheStationsSpecGeoJson, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(coucheFestivalsSpecGeoJson, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(coucheStationsRechargesSpecGeoJson, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[4].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[4].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[4].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[4].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[4].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});	
		} else if (mapContentType === "CLUSTER") {
			context.items[0] = Descartes.Utils.extend(coucheParkingsSpecClusterGeoJson, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(coucheStationsSpecClusterGeoJson, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[2].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[2].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[2].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[2].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[2].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});	
		} else if (mapContentType === "VECTORTILE") {
			
			projection = 'EPSG:3857';
			initBounds = [-800086,5055726,1150172,6772047];
			maxBounds = [-800086,5055726,1150172,6772047];
			minScale = 12000000;
            maxScale = 100;
			context.bbox = {xMin:initBounds[0], yMin:initBounds[1], xMax:initBounds[2], yMax:initBounds[3]};
			
			context.items[0] = Descartes.Utils.extend(couchePlanIgnSansStyleVectorTileSpec, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(couchePlanIgnVectorTileSpec, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(couchePlanIgnGrisVectorTileSpec, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(coucheEtalabFranceSansStyleVectorTileSpec, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(coucheEtalabFranceVectorTileSpec, {itemType:"Layer"});
			//context.items[3] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
		} else {
			context.items[0] = Descartes.Utils.extend(coucheToponymesSpecWms, {itemType:"Layer"});
			context.items[1] = Descartes.Utils.extend(groupeInfrasSpec, {itemType:"Group", items:[]});
			context.items[1].items[0] = Descartes.Utils.extend(coucheParkingsSpecWms, {itemType:"Layer"});
			context.items[1].items[1] = Descartes.Utils.extend(coucheStationsSpecWms, {itemType:"Layer"});
			context.items[1].items[2] = Descartes.Utils.extend(coucheRoutesSpecWms, {itemType:"Layer"});
			context.items[1].items[3] = Descartes.Utils.extend(coucheFerSpecWms, {itemType:"Layer"});
			context.items[2] = Descartes.Utils.extend(coucheEauSpecWms, {itemType:"Layer"});
			context.items[3] = Descartes.Utils.extend(coucheBatiSpecWms, {itemType:"Layer"});
			context.items[4] = Descartes.Utils.extend(coucheNatureSpecWms, {itemType:"Layer"});
			context.items[5] = Descartes.Utils.extend(groupeFondsSpec, {itemType:"Group", items:[]});
			context.items[5].items[0] = Descartes.Utils.extend(coucheGeoRefXYZSpec, {itemType:"Layer"});
			context.items[5].items[1] = Descartes.Utils.extend(coucheGeoRefWmtsSpec, {itemType:"Layer"});
			context.items[5].items[2] = Descartes.Utils.extend(coucheGeoRefWmsSpec, {itemType:"Layer"});
			context.items[5].items[3] = Descartes.Utils.extend(coucheOSMWmtsSpec, {itemType:"Layer"});	
			context.items[5].items[4] = Descartes.Utils.extend(coucheOSMPreconfSpec, {itemType:"Layer"});	
		}

		doMap();
	}
}

function getContext(transport) {
	
	var response = transport.responseText;
		if (response.charAt(0) !== "{") {
			//alert("La carte est disponible jusqu'au " + response.substring(0, response.indexOf("{")));
			response = response.substring(response.indexOf("{"));
		}
	context = JSON.parse(response);
	doMap();
	
}

function stopMap() {
	alert("Impossible de recharger la carte: problème lors du chargement du fichier de contexte");
}

function exampleMapContentSelectOnChange(selectItem) {
   var params = selectItem.value;
   var url = window.location.origin + window.location.pathname + "?" + params;
   if (url) {
          window.location = url; 
      }
}
function initExampleMapContentSelect() {
	var value = "moteurCarto="+moteurCarto+"&mapContentType="+mapContentType;
	var select = document.querySelector('#exampleMapContentSelect');
	var options = Array.from(select.options);
	var optionToSelect = options.find(item => item.value === value);
	optionToSelect.selected = true;

}