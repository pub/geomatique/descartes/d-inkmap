var coucheAnnotationsSpec, coucheAnnotations;
var coucheEditionGenericVectorVide1Spec, coucheEditionGenericVectorVide1;
var coucheEditionGenericVectorVide2Spec, coucheEditionGenericVectorVide2;

var myFeatures = [];

function chargeEditionCouchesGroupes() {
	
	/************************************************************************
	* 
	* COUCHE ANNNOTATIONS
	* 
	***********************************************************************/
	var typeAnnot = Descartes.Layer.EditionLayer.TYPE_Annotations;
	
	coucheAnnotationsSpec = {
		title: "Couche Annotations",
		type: typeAnnot
	}; 
	
	//coucheAnnotations = new Descartes.Layer.EditionLayer.Annotations(coucheAnnotationsSpec.title);

	/************************************************************************
	* 
	* COUCHES EDITIONS GENERIC VECTOR
	* 
	***********************************************************************/

	var typeGeneric = Descartes.Layer.EditionLayer.TYPE_GenericVector;

	coucheEditionGenericVectorVide1Spec = {
		    title: "Couche Edition Generic",
		    type: typeGeneric,
		    definition: [ {	 
		                	 loaderFunction: function persoLoaderFct(extent, resolution, projection) {
		                		 this.addFeatures(myFeatures);
		                	 }

		                 }],
		    options: {
		        alwaysVisible: false,
		        attributes: {
		            attributesEditable: [
		                {fieldName: 'Nom', label: 'Nom'}
		            ]
		        },
		        visible: true,
		        queryable: false,
		        activeToQuery: false,
		        sheetable: false,
		        opacity: 100,
		        opacityMax: 100,
		        legend: null,
		        metadataURL: null
		    }
		};

	 //coucheEditionGenericVectorVide1 = new Descartes.Layer.EditionLayer.GenericVector(coucheEditionGenericVectorVide1Spec.title, coucheEditionGenericVectorVide1Spec.definition, coucheEditionGenericVectorVide1Spec.options);

}
