/***********************************
 * 
 * 			CODE BOUCHON
 * 
 ***********************************/

function sendRequestBouchonForSaveElements(json) {
	
	if (json.fluxWfst) {
		var elementsForSave = JSON.stringify(json.fluxWfst);
		
		var xhr = new XMLHttpRequest();
	    xhr.open('POST', urlServiceBouchon);
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.onload = function () {
	        if (xhr.status === 200) {
	        	var data = xhr.responseXML;
	       	  	if(!data || !data.documentElement) {
	       	  		data = xhr.responseText;
	       	  	}
	              
	       	  	try{
	       	  		data = JSON.parse(data);
		      	  	json.priv={status:data.status,message:data.message};  
	       	  	}catch (e){
	       	  		json.priv={status:500};
	       	  	}
	       	  
	            json.callback.call(json);
	        } else {
	        	json.priv={status:500,message:xhr.responseText};
	        	json.callback.call(json);
	        }
	    };
	    xhr.send(elementsForSave);
		
	} else if (json.fluxKml){
		
		var elementsForSave = json.fluxKml; 
		delete elementsForSave.format;
		var urlwithproxy = decodeURIComponent(elementsForSave.url).split(Descartes.PROXY_SERVER);
	 	var url= urlwithproxy[urlwithproxy.length - 1];
		var splitted = url.split("/");
	 	var fileName= splitted[splitted.length - 1];
		elementsForSave.url = fileName;
		elementsForSave=JSON.stringify(elementsForSave);
		
		
	
		var xhr = new XMLHttpRequest();
	    xhr.open('POST', urlServiceBouchonKml);
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.onload = function () {
	        if (xhr.status === 200) {
	        	var data = xhr.responseXML;
	       	  	if(!data || !data.documentElement) {
	       	  		data = xhr.responseText;
	       	  	}
	              
	       	  	try{
	       	  		data = JSON.parse(data);
		      	  	json.priv={status:data.status,message:data.message};  
	       	  	}catch (e){
	       	  		json.priv={status:500};
	       	  	}
	       	  
	            json.callback.call(json);
	        } else {
	        	json.priv={status:500,message:xhr.responseText};
	        	json.callback.call(json);
	        }
	    };
	    xhr.send(elementsForSave);
	     
	}  else if (json.fluxGeoJSON){
		
		var elementsForSave = json.fluxGeoJSON; 
		delete elementsForSave.format;
		var urlwithproxy = decodeURIComponent(elementsForSave.url).split(Descartes.PROXY_SERVER);
	 	var url= urlwithproxy[urlwithproxy.length - 1];
		var splitted = url.split("/");
	 	var fileName= splitted[splitted.length - 1];
		elementsForSave.url = fileName;
		elementsForSave=JSON.stringify(elementsForSave);
		
		var xhr = new XMLHttpRequest();
	    xhr.open('POST', urlServiceBouchonGeoJSON);
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.onload = function () {
	        if (xhr.status === 200) {
	        	var data = xhr.responseXML;
	       	  	if(!data || !data.documentElement) {
	       	  		data = xhr.responseText;
	       	  	}
	              
	       	  	try{
	       	  		data = JSON.parse(data);
		      	  	json.priv={status:data.status,message:data.message};  
	       	  	}catch (e){
	       	  		json.priv={status:500};
	       	  	}
	       	  
	            json.callback.call(json);
	        } else {
	        	json.priv={status:500,message:xhr.responseText};
	        	json.callback.call(json);
	        }
	    };
	    xhr.send(elementsForSave);
		
	} else if (json.fluxSimple) {
	   if (json.fluxSimple.addObjects && json.fluxSimple.addObjects.length > 0) {
		   var addFeatures = json.fluxSimple.addObjects;
		   for (var i = 0; i < addFeatures.length; i++) {
			   var feature = new ol.Feature({
				    geometry: addFeatures[i].geometry
				});
				feature.setId(addFeatures[i].objectId);
				if (addFeatures[i].attributs) {
					feature.setProperties(addFeatures[i].attributs);
				}
			   myFeatures.push(feature);
		   }

	   } 
	   
	   if (json.fluxSimple.updatedObjects && json.fluxSimple.updatedObjects.length > 0) {
		   var updatedFeatures = json.fluxSimple.updatedObjects
		   for (var i = 0; i < updatedFeatures.length; i++) {
			   for (var j = 0; j < myFeatures.length; j++) {
				   if (myFeatures[j].getId() === updatedFeatures[i].objectId) {
					   myFeatures[j].setGeometry(updatedFeatures[i].geometry);
					   break;
				   }
			   }
		   }
		   
	   } 
	   
	   if (json.fluxSimple.removedObjects && json.fluxSimple.removedObjects.length > 0) { 
		   var removedFeatures = json.fluxSimple.removedObjects
		   var newMyFeature = [];
		   for (var i = 0; i < removedFeatures.length; i++) {
			   for (var j = 0; j < myFeatures.length; j++) {
				   if (myFeatures[j].getId() !== removedFeatures[i].objectId) {
					   newMyFeature.push(myFeatures[j]);
				   }
			   }
		   }
		   myFeatures = newMyFeature;
	   } 
	   
	   json.priv={status:200,message:"Sauvegarde effectuée"}; 
	   json.callback.call(json);  
   } else {
	   json.priv={status:500,message:"erreur lors de la sauvegarde"};
	   json.callback.call(json); 
   }
   
   
	 
}

