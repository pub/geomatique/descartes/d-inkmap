const path = require('path');

var hostname = '127.0.0.1';
var port = 8087;


var root = path.resolve(__dirname, '/');

module.exports = {
    entry: 'mocha-loader\!./tests/index.js',
    output: {
        filename: 'test.build.js',
        path: path.resolve(root, '/tests'),
        publicPath: 'http://' + hostname + ':' + port + '/tests'
    },
    module: {
        loaders: [{
                test: /(\.css|\.less)$/,
                loader: 'null-loader',
                exclude: [
                    /build/
                ]
            }, {
                test: /(\.jpg|\.jpeg|\.png|\.gif)$/,
                loader: 'null-loader'
            }]
    },
    devtool: 'source-map',
    devServer: {
        host: hostname,
        port: port
    }
};