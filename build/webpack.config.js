/* global __dirname */

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

let root = path.resolve(__dirname, '../');

module.exports = function (ui) {
    return {
        entry: './src/dinkmap_index',
        output: {
            path: path.resolve(root, './dist'),
            publicPath: '../',
            filename: 'd-inkmap.js',
            library: 'Descartes',
            libraryTarget: 'umd',
            umdNamedDefine: true
        },
        devtool: 'source-map',
        module: {
            rules: [{
                enforce: 'pre',
                test: /\.js$/,
                loader: 'eslint-loader',
                options: {
                    quiet: true //show or not warnings
                }
            }, {
                test: /\.(jpe?g|gif|png|svg)$/,
                loader: 'file-loader?name=[name].[ext]&outputPath=img/'
            }, {
                test: /\.ejs$/,
                loader: 'ejs-loader'
            }, {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: 'css-loader'
                })
            }]
        },
        externals: {
            "lodash": {
                commonjs: "lodash",
                commonjs2: "lodash",
                amd: "lodash",
                root: "_"
            },
            "jquery": {
                commonjs: "jQuery",
                commonjs2: "jQuery",
                amd: "jQuery",
                root: "$"
            },
            "jspdf": {
                commonjs: "jspdf",
                commonjs2: "jspdf",
                amd: "jspdf",
                root: "jspdf"
            },
            "@camptocamp/inkmap": {
                commonjs: "@camptocamp/inkmap",
                commonjs2: "@camptocamp/inkmap",
                amd: "@camptocamp/inkmap",
                root: "@camptocamp/inkmap"
            }
        },
        plugins: [
            new webpack.DefinePlugin({
                MODE: JSON.stringify(ui)
            }),
            new webpack.BannerPlugin({
            	  banner: 'Descartes - ' + JSON.stringify(require("../package.json").name) + ' -  ' + JSON.stringify(require("../package.json").version) 
            }),
            new ExtractTextPlugin('css/d-inkmap.css')
        ]
    }
};
