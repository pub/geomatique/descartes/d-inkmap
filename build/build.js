const webpack = require('webpack');
const webpackConf = require('./webpack.config.js');
const argv = require('./common').argv;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

// get conf for current options
const conf = webpackConf(argv.ui, argv.georef);

// add uglify plugin
conf.plugins.push(new UglifyJsPlugin({
    sourceMap: true,
    test: /\.js$/,
    parallel: true
}));

console.log('Launching webpack build...');
webpack(conf, function (err, stats) {
    if (err)
        throw err;
    process.stdout.write(stats.toString({
        colors: true,
        modules: false,
        children: false,
        chunks: false,
        chunkModules: false
    }) + '\n');
});
