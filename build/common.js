const path = require('path');
const fs = require('fs');
const exec = require('child_process').exec;
require('shelljs/global');
const utils = require('./utils.js');

// à compléter selon l'implémentation de nouveau mode
const availableModes = ['bootstrap4'];

const argv = require('yargs')
    .help('h')
    .alias('h', 'help')
    .option('ui', {
        type: 'string',
        description: 'UI mode to select',
        default: availableModes[0],
        choices: availableModes
    })
    .option('port', {
        type: 'number',
        default: 4202,
        description: 'Port on which to run dev server',
    }).argv;

console.log('Using UI mode:', argv.ui);

const root = path.resolve(__dirname, '../');
const nodeModules = path.resolve(root, 'node_modules');
const distPath = path.resolve(root, 'dist');
const vendorPath = path.resolve(distPath, 'vendor');

rm('-rf', distPath);
console.log('Dist folder emptied');

fs.mkdirSync(distPath);
fs.mkdirSync(vendorPath);

console.log('Concatenating vendor files (JS and CSS)...');
const jsFiles = [
    path.resolve(nodeModules, 'jspdf/dist/jspdf.umd.min.js'),
    path.resolve(nodeModules, 'file-saver/dist/FileSaver.min.js'),
    path.resolve(nodeModules, '@webcomponents/custom-elements/custom-elements.min.js'),
    //add inkmap lib to vendors
    path.resolve(nodeModules, '@camptocamp/inkmap/dist/inkmap.js')
];
const cssFiles = [

];
utils.concatFiles(jsFiles, path.resolve(vendorPath, 'd-inkmap-required.js'));
utils.concatFiles(cssFiles, path.resolve(vendorPath, 'd-inkmap-required.css'));
//copy inkmap service worker to the dist root
utils.copyFile(path.resolve(nodeModules, '@camptocamp/inkmap/dist/inkmap-worker.js'), path.resolve(distPath, 'inkmap-worker.js'));
utils.copyFile(path.resolve(nodeModules, '@camptocamp/inkmap/dist/inkmap-worker.js.map'), path.resolve(distPath, 'inkmap-worker.js.map'));

module.exports = {
    argv
};
