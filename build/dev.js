const path = require('path');
const webpack = require('webpack');
const webpackConf = require('./webpack.config.js');
const webpackDevServer = require('webpack-dev-server');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const argv = require('./common').argv;

// get conf for current options
const conf = webpackConf(argv.ui, argv.georef);

// add entry to enable live reload & add bundle analyzer
conf.entry = ['webpack-dev-server/client?http://localhost:' + argv.port, conf.entry];
conf.plugins = conf.plugins.concat([
    new BundleAnalyzerPlugin({
        openAnalyzer: false,
        analyzerPort: 4102
    })
]);

// info du serveur JAVA ou PHP servant Descartes coté serveur
var descartesServer = {
    protocol: 'http',
    host: 'localhost',
    port: 8080,
    path: '/services/api/v1/'
};

const server = new webpackDevServer(webpack(conf), {
    proxy: {
        //on a donc un proxy de proxy.... (pour le dev uniquement)
        '/*/proxy': {
            target: descartesServer,
            pathRewrite: {
                '/.*/proxy': '/proxy'
            }
        },
        // this is so that all examples can find the inkmap worker without copying it everywhere
        '/examples/*/inkmap-worker.js': {
            target: `http://localhost:${argv.port}`,
            pathRewrite() { return '/inkmap-worker.js'; }
        }
    },
    inline: true,
    contentBase: [
        path.resolve(__dirname, '../node_modules' ),
        path.resolve(__dirname, '../dist' ),
        path.resolve(__dirname, '../demo')
    ],
    quiet: false,
    noInfo: false,
    publicPath: '/',
    stats: {
        colors: true
    },
    watchContentBase: true,
    port: argv.port
});

console.log('Starting webpack-dev-server...');
server.listen(argv.port, 'localhost', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Dev server started on http://localhost:' + argv.port);
    }
});
