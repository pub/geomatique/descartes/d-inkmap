/* global Descartes*/

var _ = require('lodash');
var Utils = Descartes.Utils;
var DescartesPlug = require('./DInkMap/DInkMap');

Descartes.Map._TOOLS_WITH_MAPCONTENT.push('Descartes.Button.InkMapExportPNG');
Descartes.Map._TOOLS_WITH_MAPCONTENT.push('Descartes.Action.InkMapParamsManager');

_.extend(Descartes.Action, DescartesPlug.Action);
_.extend(Descartes.Button, DescartesPlug.Button);
_.extend(Descartes.UI, DescartesPlug.UI);
_.extend(Descartes.Messages, DescartesPlug.Messages);

module.exports = Descartes;
