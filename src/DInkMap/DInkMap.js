/* global MODE */

//Action
var InkMapParamsManager = require('./Action/InkMapParamsManager');

//Button
var InkMapExportPNG = require('./Button/InkMapExportPNG');

//UI
var AbstractInkMapSetup = require('./UI/' + MODE + '/AbstractInkMapSetup');
var InkMapSetupInPlace = require('./UI/' + MODE + '/InkMapSetupInPlace');

//Utils
var UtilsInkMap = require('./Utils/UtilsInkMap');

var Messages = require('./Messages');

var DInkMap = {
	Action: {InkMapParamsManager: InkMapParamsManager},
	Button: {InkMapExportPNG: InkMapExportPNG},
	Messages: Messages,
	UI: {AbstractInkMapSetup: AbstractInkMapSetup, InkMapSetupInPlace: InkMapSetupInPlace},
	Utils: {UtilsInkMap: UtilsInkMap}
};

module.exports = DInkMap;
