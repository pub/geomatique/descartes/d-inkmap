var _ = require('lodash');

var messages = {

    Descartes_Messages_Button_InkMapExportPNG: {
        TITLE: "Enregistrement PNG",
        DIALOG_TITLE: "Paramètres de la mise en page"
    },

    Descartes_Messages_Button_InkMapExportPDF: {
        TITLE: "Mise en page PDF"
    },
    Descartes_Forms_InkMapSetup: {
        LANDSCAPE_OPTION: " Paysage",
        PORTRAIT_OPTION: " Portrait",
        PAPER_INPUT: "Format de feuille : ",
        DPI_INPUT: "Résolution : ",
        PROJECTION_INPUT: "Projection : ",
        MARGINS_GROUP_INPUT: "Marges (en mm) : ",
        MARGIN_TOP_INPUT: "Haut : ",
        MARGIN_BOTTOM_INPUT: "Bas : ",
        MARGIN_LEFT_INPUT: "Gauche : ",
        MARGIN_RIGHT_INPUT: "Droit : ",
        MARGIN_TOP_ERROR: "La valeur de la marge \"haut\" doit être un nombre positif",
        MARGIN_BOTTOM_ERROR: "La valeur de la marge \"bas\" doit être un nombre positif",
        MARGIN_LEFT_ERROR: "La valeur de la marge \"gauche\" doit être un nombre positif",
        MARGIN_RIGHT_ERROR: "La valeur de la marge \"droit\" doit être un nombre positif",
        MARGIN_WIDTH_ERROR: "En largeur, il manque pour inclure la carte ",
        MARGIN_HEIGHT_ERROR: "En hauteur, il manque pour inclure la carte ",
        MARGINS_TIP: ". Diminuez les marges.",
        ERRORS_LIST: "La génération du PDF ne peut être déclenchée car les erreurs suivantes ont été rencontrées :",
        PRINT_LEGEND: "Légende",
        PRINT_PROJECTION: "Système de projection",
        PRINT_SCALE: "Échelle",
        PRINT_NORTH: "Indication Nord",
        PRINT_TITLE: "Titre",
        PRINT_DESCRIPTION: "Description",
        PRINT_DATE: "Date d'impression",
        PRINT_QR: "QR Code",
        SHOW_PREVIEW_MESSAGE: "Activer l'outil d'impression"
    },

    Descartes_Messages_UI_InkMapSetupInPlace: {
        TITLE_MESSAGE: "Paramètres de la mise en page",
        BUTTON_MESSAGE: "Générer le PDF"
    },
    Descartes_Messages_UI_InkMapSetupDialog: {
        DIALOG_TITLE: "Paramètres de la mise en page",
        OK_BUTTON: "Générer le PDF",
        CANCEL_BUTTON: "Annuler"
    }

};

messages.Descartes_Messages_UI_InkMapSetupInPlace = _.extend(messages.Descartes_Messages_UI_InkMapSetupInPlace, messages.Descartes_Forms_InkMapSetup);
messages.Descartes_Messages_UI_InkMapSetupDialog = _.extend(messages.Descartes_Messages_UI_InkMapSetupDialog, messages.Descartes_Forms_InkMapSetup);

module.exports = messages;
