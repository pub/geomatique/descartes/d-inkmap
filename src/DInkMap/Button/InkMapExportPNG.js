/* global Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;
var Button = Descartes.Button;

require('./css/InkMapExportPNG.css');

/**
 * Class: Descartes.Button.InkMapExportPNG
 * Classe définissant un bouton permettant d'exporter la carte courante sous forme de fichier PNG.
 *
 * Hérite de:
 *  - <Descartes.Button>
 */
var Class = Utils.Class(Button, {
    /**
     * Propriete: defaultInfos
     * {Object} Objet JSON stockant les informations complémentaires par défaut à insérer dans le fichier PDF.
     *
     * :
     * title - Titre de la carte (utilisé pour le nom du fichier généré).
     * copyright - Texte du copyright à insérer en sur-impression de l'image.
     */
    defaultInfos: {
        title: 'Descartes',
        copyright: ''
    },
    /**
     * Propriete: infos
     * {Object} Objet JSON stockant les informations complémentaires à insérer dans le fichier PNG.
     *
     * :
     * Construit à partir de defaultInfos, puis surchargé en présence d'options complémentaires.
     */
    infos: null,

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,
    /**
     * Constructeur: Descartes.Button.ExportPNG
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * infos - Objet JSON stockant les informations complémentaires à insérer dans le fichier PNG.
     */
    initialize: function (options) {
        this.enabled = true;
        this.infos = _.extend({}, this.defaultInfos);
        if (!_.isNil(options) && !_.isNil(options.infos)) {
            _.extend(this.infos, options.infos);
            delete options.infos;
        }
        Button.prototype.initialize.apply(this, arguments);
    },
    /**
     * Methode: execute
     * Lance la génération du PNG grâce au service d'exportation PNG.
     */
    execute: async function () {
        if (!_.isNil(this.mapContent)) {
            var layers = this.mapContent.getVisibleLayers().sort(function (a, b) {
                return b.displayOrder - a.displayOrder;
            });
            var layersSpec = { layers: await Utils.writeInkmapLayers(layers, this.olMap) };
            var partialSpec = Utils.writeInkmapPartialSpec(this.olMap);
            var printspec = Object.assign(partialSpec, layersSpec);
            Utils.exportPNG(printspec);
        }
    },
    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },
    CLASS_NAME: 'Descartes.Button.InkMapExportPNG'
});

module.exports = Class;
