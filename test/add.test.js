var assert = require('assert');


describe('Demo', () => {
    it('should add correctly', () => {
        assert.equal(1 + 1, 2);
    });
    it('should remove correctly', () => {
        assert.equal(1 - 1, 0);
    });
});
