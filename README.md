# D-InkMap, composant Javascript

Le composant Javascript D-InkMap est le module d'impression de Descartes.

Ce composant est basé sur la librairie d'impression InkMap https://github.com/camptocamp/inkmap
 
## Exemples de démonstration

https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-inkmap
 
## Paquet NPM

Le composant Javascript D-InkMap est disponible sous forme de paquet NPM sous l'appellation `@descartes/d-inkmap`.

### Utilisation dans une application tierce

#### Utilisation via NPM

##### Installation

Pour installer le composant JS D-inkmap dans un projet NPM :
```bash
$ npm install @descartes/d-inkmap
```

Note : le paquet est disponible sur différents dépôts, il faut donc faire pointer le scope `@descartes` sur le dépôt souhaité:
- dépôt public gitlab-forge (pour les versions diffusées au sein du ministère MTE) :  
  https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm 
- dépôt privé (pour les versions en cours de développement) :  
  https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/8484/packages/npm 
- dépôts public NPM (pour les versions diffusées "grand public" à accès CDN)
  https://registry.npmjs.org

Pour que NPM puisse se connecter aux dépôts, il faut renseigner le fichier `.npmrc` :
```
@descartes:registry=https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm
...
```

##### Inclusion du composant

Tout d'abord, le chargement de la librairie principale Descartes est nécessaire:
le paquet `@descartes/d-map` doit être inclus dans l'application. 

Pour utiliser le composant JS D-InkMap, il suffit d'inclure les fichiers suivants dans la page HTML de l'application (les chemins relatifs doivent être respectés) :
* `vendor/d-inkmap-required.css`
* `vendor/d-inkmap-required.js`
* `css/d-inkmap.css`
* `d-inkmap.js`

#### Utilisation via CDN

Tout d'abord, le chargement de la librairie principale Descartes est nécessaire:
les urls CDN de `@descartes/d-map` doit être incluses dans l'application. 

Pour utiliser le composant JS D-Map, il suffit d'inclure ensuite les fichiers suivants dans la page HTML de l'application  :
* `https://cdn.jsdelivr.net/npm/@descartes/d-inkmap@X.X.X/dist/vendor/d-inkmap-required.css`
* `https://cdn.jsdelivr.net/npm/@descartes/d-inkmap@X.X.X/dist/vendor/d-inkmap-required.js`
* `https://cdn.jsdelivr.net/npm/@descartes/d-inkmap@X.X.X/dist/css/d-inkmap.css`
* `https://cdn.jsdelivr.net/npm/@descartes/d-inkmap@X.X.X/dist/d-inkmap.js`

Note : l'utilisation via CDN n'est pas recommandé en production.



### Utilisation du composant en cours de développement

#### Pre-requis

Installation sur son poste:
* NODE 16.14.0
* NPM 8.5.2

#### Utilisation de link

Il est possible de travailler simultanément sur le composant d-inkmap et sur une application tierce, afin de vérifier
par exemple le bon comportement d'une évolution de d-inkmap.

Pour cela, on commence par demander à npm de créer une référence locale globale du paquet, en se positionnant à la racine du projet d-inkmap :
```
npm link
```

Ensuite, il faut indiquer à NPM que le paquet `@descartes/d-inkmap` doit être résolu localement, en se positionnant dans le projet tierce :
```
$ npm link @descartes/d-inkmap
```

Une fois les développements terminés, pour revenir à un état normal, en se positionnant dans le projet tierce :
```
npm unlink --no-save @descartes/d-inkmap
npm install
```

Finalement, on peut demander à npm de nettoyer la référence locale globale, en se positionnant à la racine du projet d-inkmap :
```
npm unlink
```

#### Compilation

La compilation du composant JS de D-InkMap se fait à l'aide de Webpack. Par ailleurs le composant possède les dépendances suivantes :
* jsPdf
* inkmap

Pour compiler le composant :
```bash
$ npm run build
```

Lors de la compilation, les fichiers suivants sont générés dans le répertoire `dist` à la racine de ce module :
* `d-inkmap.js` et `d-inkmap.css` : contiennent le code source et les feuilles de style utilisées par D-InkMap
  > ces fichiers sont générés par Webpack
* le répertoire `vendor` contient les fichiers `d-inkmap-required.js` et `d-inkmap-required.css`; ces deux fichiers contiennent l'intégralité des dépendances requises par D-InkMap
  > ces fichiers sont générés par le script `build/build.js`
* tous les autre fichiers (images, polices, ...) sont des ressources requises pour le bon fonctionnement du composant

#### Développement du composant

Pour lancer les exemples D-InkMap :
```bash
$ npm run dev
```

Si besoin, pour spécifier un port : `npm run dev -- --port=XXXX`. 
Le port par défaut est `4202`.

L'index des exemples est accessible sur http://localhost:4202. Les exemples se rafraichissent automatiquement en cas de changement dans les fichiers source.

> Note : Le répertoire `dist` ainsi que les répertoires `/demo` et `/node_modules` sont rendus accessibles à la racine du serveur.

#### Publication

La publication du paquet NPM se fait via GITLAB-CI à la création d'un TAG.
